// Imports
var express     = require('express');
var bodyParser  = require('body-parser');
var apiRouter   = require('./apiRouter').router;
var servers = require('http').createServer(express); //passed to http server
var io = require('socket.io')(servers); //http server passed to socket.io

const SocketManager = require('./routes/SocketIoCtr.js');

//Listen connexion socket io
io.on('connection', SocketManager)

// Instantiate server
var server = express();

// Body Parser configuration
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

// Configure routes
server.get('/', function (req, res) {
    res.setHeader('Content-Type', 'text/html');
    res.status(200).send(' <h1>Waooo Bienvenue sur mon Serveur!</h1>');
});

server.use('/api/', apiRouter);

// Launch server
server.listen(3030, function () {
    console.log('Server API Rest en écoute :)');
});

// Launch server
servers.listen(3000, function () {
    console.log('Server SOCKET IO en écoute :)');
});



















