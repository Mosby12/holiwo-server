// Imports

var express      = require('express');
var usersCtrl    = require('./routes/usersCtrl');
var adminCtrl    = require('./routes/adminCtrl');
var publicationCtrl = require('./routes/publicationCtrl');
var likesCtrl    = require('./routes/likesCtrl');
var categorieCtrl    = require('./routes/categorieCtrl');
var currencyCtrl    = require('./routes/currencyCtrl');
var followCtrl = require('./routes/followCtrl');
var chatCtrl = require('./routes/chatCtrl');
var path = require('path');
const multer = require('multer');
const DIR = './imag/publication';

// Router
exports.router = (function() {
 let storage = multer.diskStorage({
  destination: function (req, file, callback) {
   callback(null, DIR);
  },
  filename: function (req, file, cb) {
   cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
 });
 let upload = multer({storage: storage});

 var apiRouter = express.Router();

  // Users routes
  apiRouter.route('/users/register/').post(usersCtrl.register);
  apiRouter.route('/users/login/').post(usersCtrl.login);
  apiRouter.route('/users/me/').get(usersCtrl.getUserProfile);

 apiRouter.route('/users/update/profil/me/').put(usersCtrl.updateUserProfile);
 apiRouter.route('/users/profile/update/state/').put(usersCtrl.updateUserProfileState);
 apiRouter.route('/users/profile/search/:idUser/').get(usersCtrl. searchUserProfile);
 apiRouter.route('/users/profile/password/edit/').put(usersCtrl. updateUserProfilePassword);

 // admin routes
 apiRouter.route('/admin/register/').post(adminCtrl.register);
 apiRouter.route('/admin/login/').post(adminCtrl.login);
 apiRouter.route('/admin/me/').get(adminCtrl.getUserProfile);

 apiRouter.route('/admin/update/profile/me/').put(adminCtrl.updateAdminProfile);
 apiRouter.route('/admin/profile/update/state/').put(adminCtrl.updateAdminProfileState);
 apiRouter.route('/admin/profile/search/:idAdmin/').get(adminCtrl. searchAdminProfile);
 apiRouter.route('/admin/profile/password/edit/').put(adminCtrl. updateUserProfilePassword);

 //publication routes
 apiRouter.route('/publication/new/').post(publicationCtrl.createPublication);
 apiRouter.post('/publication/new2/', upload.single("imag"), publicationCtrl.createPublication2);
 apiRouter.route('/publication/:page').get(publicationCtrl.listPublication);
 apiRouter.route('/publication/search/').post(publicationCtrl.searchPublication);
 // Likes
 apiRouter.route('/publication/:publishId/vote/like/').post(likesCtrl.likePost);
 apiRouter.route('/publication/:publishId/vote/dislike/').post(likesCtrl.dislikePost);
 apiRouter.route('/publication/:publishId/action/').get(likesCtrl.getUserAction);

 //categorie
 apiRouter.route('/categorie/save/').post(categorieCtrl.saves);
 apiRouter.route('/categorie/update/').put(categorieCtrl.updateCategorie);
 apiRouter.route('/categorie/currency/').get(categorieCtrl.getCategorieById);

 //currency
 apiRouter.route('/currency/save/').post(currencyCtrl.saves);
 apiRouter.route('/currency/update/').put(currencyCtrl.updateCurrency);
 apiRouter.route('/currency/getCurrency/').get(currencyCtrl.getCurrencyById);

 //follow
 apiRouter.route('/followers/:adminId/follow/').post(followCtrl.followAdmin);
 apiRouter.route('/followers/:adminId/unfollow/').post(followCtrl.unfollowAdmin);

//Chat
 apiRouter.route('/chat/user/message/all/').get(chatCtrl.getUserChatList);
 apiRouter.route('/chat/user/send/message/').post(chatCtrl.ChatUserPost);
 apiRouter.route('/chat/user/message/').post(chatCtrl.AllUserChatmessage);
 apiRouter.route('/chat/admin/message/all').get(chatCtrl.getAdminChatList);
 apiRouter.route('/chat/admin/send/message/').post(chatCtrl.ChatAdminPost);
 apiRouter.route('/chat/admin/message/').post(chatCtrl.AllAdminChatmessage);

 //img
 apiRouter.get('/imag/publication/:imag', function (req, res) {
  var name = "imag/publication/" + req.params.imag;
  res.sendfile(path.resolve(path.resolve(__dirname, name)));
 });

  return apiRouter;
})();
