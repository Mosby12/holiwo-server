'use strict';
module.exports = (sequelize, DataTypes) => {
    const currency = sequelize.define('Currency', {
        currencyName: DataTypes.STRING,
        description: DataTypes.STRING,
        symbol: DataTypes.STRING,
        state: DataTypes.INTEGER
    }, {});
    currency.associate = function(models) {
        // associations can be defined here
    };
    return currency;
};
