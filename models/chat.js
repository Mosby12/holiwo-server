'use strict';
module.exports = (sequelize, DataTypes) => {
  const Chat = sequelize.define('Chat', {
    AdminId:{ type:DataTypes.INTEGER,
    references:{
      model:'Admin',
      key:"id"
    }},
    UserId:{
      type: DataTypes.INTEGER,
      references:{
        model:'Users',
        key:"id"
      }
    },
    PublicationId:{
      type:DataTypes.INTEGER,
      references:{
        model:'Publication',
        key:'id'
      }
    },
    attachment:DataTypes.STRING,
    typeSender: DataTypes.STRING,
    contentText: DataTypes.STRING
  }, {});
  Chat.associate = function(models) {
    // associations can be defined here

    models.Users.belongsToMany(models.Admin, {
      through: models.Like,
      foreignKey: 'UserId',
      otherKey: 'AdminId',
    });

    models.Admin.belongsToMany(models.Users, {
      through: models.Like,
      foreignKey: 'AdminId',
      otherKey: 'UserId',
    });

    models.Chat.belongsTo(models.Users, {
      foreignKey: 'UserId',
    });

    models.Chat.belongsTo(models.Admin, {
      foreignKey: 'AdminId',

    });
    models.Publication.hasMany(models.Chat, {
      foreignKey: 'PublicationId',
    });

  };
  return Chat;
};
