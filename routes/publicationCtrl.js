// Imports
var models   = require('../models');
var asyncLib = require('async');
var jwtUtils = require('../utils/jwt-admin.utils');

// Constants
/*const TITLE_LIMIT   = 2;
const CONTENT_LIMIT = 4;*/
const ITEMS_LIMIT   = 50;
const Sequelize = require('sequelize');
const URL = "imag/publication/"
const Op = Sequelize.Op;

// Routes
module.exports = {

  createPublication: function(req, res) {
    // Getting auth header
    var headerAuth  = req.headers['authorization'];
    var idAdmin      = jwtUtils.getidAdmin(headerAuth);

    // Params
    var rate   = req.body.rate;
    var content = req.body.content;
    var categorieId =req.body.categorieId;
    var attachment=req.body.attachment;
    var price=req.body.price;
    var currencyId =req.body.currencyId;
    var title =req.body.title;

    if (rate == null || content == null|| categorieId == null) {
      return res.status(400).json({ 'error': 'missing parameters' });
    }

  /*  if (rate.length <= TITLE_LIMIT || content.length <= CONTENT_LIMIT) {
      return res.status(400).json({ 'error': 'invalid parameters' });
    }*/

    asyncLib.waterfall([
      function(done) {
        models.Admin.findOne({
          where: [{id: idAdmin}, {state: 0}]
        })
        .then(function(userFound) {

          done(null, userFound);
        })
        .catch(function(err) {
          return res.status(500).json({ 'error': 'unable to verify admin' });
        });
      },

      function(userFound, done) {
        if(userFound) {
          models.Publication.create({
            rate  : rate,
            content: content,
            state:0,
            likes  : 0,
            AdminId: userFound.id,
            CategorieId:categorieId,
            attachment:attachment,
            price:price,
            CurrencyId:currencyId,
            title:title
          })
          .then(function(newPublication) {
            done(newPublication);
          });
        } else {
          res.status(404).json({ 'error': 'user not found' });
        }
      },
    ], function(newPublication) {
      if (newPublication) {
        return res.status(201).json(newPublication);
      } else {
        return res.status(500).json({'error': 'cannot post publication'});
      }
    });
  },


    createPublication2: function (req, res) {
        // Getting auth header
        var headerAuth = req.headers['authorization'];
        var idAdmin = jwtUtils.getidAdmin(headerAuth);

        // Params
        var rate = req.body.rate;
        var content = req.body.content;
        var categorieId = req.body.categorieId;
        //var attachment=req.body.attachment;
        var price = req.body.price;
        var currencyId = req.body.currencyId;
        var title = req.body.title;
        var attachment = URL + req.file.filename;
        var attachment1 = URL + req.file.filename;
        ;
        var attachment2 = URL + req.file.filename;

        if (rate == null || content == null || categorieId == null) {
            return res.status(400).json({'error': 'missing parameters'});
        }


        /* if (rate.length <= TITLE_LIMIT || content.length <= CONTENT_LIMIT) {
           return res.status(400).json({ 'error': 'invalid parameters' });
         }*/

        if (!req.file) {
            console.log(" File non reçu");
        }

        asyncLib.waterfall([
            function (done) {
                models.Admin.findOne({
                    where: [{id: idAdmin}, {state: 0}]
                })
                    .then(function (userFound) {

                        done(null, userFound);
                    })
                    .catch(function (err) {
                        return res.status(500).json({'error': 'unable to verify admin'});
                    });
            },

            function (userFound, done) {
                if (userFound) {
                    models.Publication.create({
                        rate: rate,
                        content: content,
                        state: 0,
                        likes: 0,
                        AdminId: userFound.id,
                        CategorieId: categorieId,
                        price: price,
                        CurrencyId: currencyId,
                        title: title,
                        attachment: attachment,
                        attachment1: attachment1,
                        attachment2: attachment2,
                    })
                        .then(function (newPublication) {
                            done(newPublication);
                        });
                } else {
                    res.status(404).json({'error': 'user not found'});
                }
            },
        ], function (newPublication) {
            if (newPublication) {
                return res.status(201).json(newPublication);
            } else {
                return res.status(500).json({'error': 'cannot post publication'});
            }
        });
    },
  listPublication: function(req, res) {
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;

      var limit = 50;   // number of records per page
      var offset = 0;
      var page = req.params.page;      // page number



    if (limit > ITEMS_LIMIT) {
      limit = ITEMS_LIMIT;
    }

    models.Publication.findAll({
        /* order: [(order != null) ? order.split(':') : ['createdAt', 'ASC']],*/
        order: [(order != null) ? order.split(':') : ['createdAt', 'DESC']],
      attributes: (fields !== '*' && fields != null) ? fields.split(',') : null,
      limit: (!isNaN(limit)) ? limit : null,
        /*   offset: (!isNaN(offset)) ? offset : null,*/
        offset: limit * (page - 1),
      include: [{
        model: models.Admin,
          attributes: ['username', 'propic'],
      },
        {
        model: models.Categorie,
        attributes: [ 'categorieName'],
      }, {
          model: models.Currency,
          attributes: [ 'symbol'],
        }]
    }).then(function(publication) {
      if (publication) {
        res.status(200).json(publication);
      } else {
        res.status(404).json({"error": "no publication found"});
      }
    }).catch(function(err) {
      console.log(err);
      res.status(500).json({ "error": "invalid fields" });
    });
  },
  searchPublication: function(req, res) {
    var fields  = req.query.fields;
    var limit   = parseInt(req.query.limit);
    var offset  = parseInt(req.query.offset);
    var order   = req.query.order;
    var searchWord =  req.body.searchWord;

    if (limit > ITEMS_LIMIT) {
      limit = ITEMS_LIMIT;
    }

      models.Publication.findAll({
      attributes: [ 'id', 'mail', 'username', 'state','name', 'firstname', 'propic', 'tel', 'address','town','title','position','state','createdAt',"updatedAt" ],
      where: [{
        [Op.or]: [{
          title: {
            [Op.like]: '%' + searchWord + '%'
          }
        }, {
          price: {
            [Op.like]: '%' + searchWord + '%'
          }
        }, {
          title: {
            [Op.like]: '%' + searchWord + '%'
          }
        }, {
          content: {
            [Op.like]: '%' + searchWord + '%'
          }
        }]
      }, {state: 0}],
      order: [(order != null) ? order.split(':') : ['createdAt', 'ASC']],
      attributes: (fields !== '*' && fields != null) ? fields.split(',') : null,
      limit: (!isNaN(limit)) ? limit : null,
      offset: (!isNaN(offset)) ? offset : null,
      include: [{
          model: models.Admin,
          attributes: ['username', 'propic'],
      },
          {
              model: models.Categorie,
              attributes: ['categorieName'],
          }, {
              model: models.Currency,
              attributes: ['symbol'],
          }]
    }).then(function(publication) {
      if (publication) {
        res.status(200).json(publication);
      } else {
        res.status(404).json({"error": "no publication found"});
      }
    }).catch(function(err) {
      console.log(err);
      res.status(500).json({ "error": "invalid fields" });
    });
  }
}
