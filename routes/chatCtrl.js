// Imports
var models = require('../models');
var jwt_User_Utils = require('../utils/jwt-user.utils');
var jwt_Admin_Utils = require('../utils/jwt-admin.utils');
var asyncLib = require('async');

// Constants
const USER = 0;
const ADMIN = 1;

// Routes
module.exports = {
    ChatUserPost: function (req, res) {
        // Getting auth header
        var headerAuth = req.headers['authorization'];
        var userId = jwt_User_Utils.getidUser(headerAuth);

        // Params

        var AdminId = parseInt(req.body.AdminId);
        var PublicationId = parseInt(req.body.PublicationId);
        var contentText = req.body.contentText;
        var attachment = req.body.attachment;

        if (PublicationId <= 0) {
            return res.status(400).json({'error': 'invalid parameters'});
        }

        asyncLib.waterfall([
            function (done) {
                models.Publication.findOne({
                    where: {id: PublicationId}

                })
                    .then(function (messageFound) {
                        done(null, messageFound);
                    })
                    .catch(function (err) {
                        return res.status(500).json({'error': 'unable to verify message'});
                    });
            },
            function (messageFound, done) {
                if (messageFound) {
                    models.Users.findOne({
                        where: {id: userId}
                    })
                        .then(function (userFound) {
                            done(null, messageFound, userFound);
                        })
                        .catch(function (err) {
                            return res.status(500).json({'error': 'unable to verify user'});
                        });
                } else {
                    res.status(404).json({'error': 'post already liked'});
                }
            },
            function (messageFound, userFound, done) {
                if (userFound) {
                    models.Admin.findOne({
                        where: {
                            id: AdminId
                        }
                    })
                        .then(function (adminFound) {
                            done(null, messageFound, userFound, adminFound);
                        })
                        .catch(function (err) {
                            return res.status(500).json({'error': 'unable to verify is user already liked'});
                        });
                } else {
                    res.status(404).json({'error': 'user not exist'});
                }
            },
            function (messageFound, userFound, adminFound, done) {
                if (adminFound) {

                    models.Chat.create({
                        AdminId: AdminId,
                        UserId: userId,
                        PublicationId: PublicationId,
                        typeSender: USER,
                        contentText: contentText,
                        attachment: attachment,
                        state: 0
                    })

                        .then(function (alreadyLikeFound) {
                            done(alreadyLikeFound);
                        })
                        .catch(function (err) {
                            return res.status(500).json({'error': 'unable to set user reaction'});
                        });
                } else {

                }
            },

        ], function (alreadyLikeFound) {
            if (alreadyLikeFound) {
                return res.status(201).json(alreadyLikeFound);
            } else {
                return res.status(500).json({'error': 'cannot update message'});
            }
        });
    },

    getUserChatList: function (req, res) {

        var headerAuth = req.headers['authorization'];
        var userId = jwt_User_Utils.getidUser(headerAuth);

        var fields = req.query.fields;

        var offset = parseInt(req.query.offset);
        var order = req.query.order;

        if (userId <= 0 || userId == null) {
            return res.status(400).json({'error': 'Identification failed'});
        }

        models.Chat.findAll({
            order: [(order != null) ? order.split(':') : ['createdAt', 'ASC']],
            attributes: (fields !== '*' && fields != null) ? fields.split(',') : null,
            include: [{
                model: models.Admin,
                attributes: ['username'],
            }],
            offset: (!isNaN(offset)) ? offset : null,

            where: {UserId: userId},

        }).then(function (publication) {
            if (publication) {
                res.status(200).json(publication);
            } else {
                res.status(404).json({"error": "no messages found"});
            }
        }).catch(function (err) {
            console.log(err);
            res.status(500).json({"error": "invalid fields"});
        });
    },

    AllUserChatmessage: function (req, res) {
        var headerAuth = req.headers['authorization'];
        var userId = jwt_User_Utils.getidUser(headerAuth);

        var fields = req.query.fields;

        var offset = parseInt(req.query.offset);
        var order = req.query.order;
        var PublicationId = parseInt(req.body.PublicationId);
        var AdminId = parseInt(req.body.AdminId);

        models.Chat.findAll({
            order: [(order != null) ? order.split(':') : ['createdAt', 'ASC']],
            attributes: (fields !== '*' && fields != null) ? fields.split(',') : null,

            offset: (!isNaN(offset)) ? offset : null,
            where: [{UserId: userId}, {PublicationId: PublicationId}, {AdminId: AdminId}],

        }).then(function (publication) {
            if (publication) {
                res.status(200).json(publication);
            } else {
                res.status(404).json({"error": "no messages found"});
            }
        }).catch(function (err) {
            console.log(err);
            res.status(500).json({"error": "invalid fields"});
        });
    },

    ChatAdminPost: function (req, res) {
        // Getting auth header

        var headerAuth = req.headers['authorization'];
        var adminId = jwt_User_Utils.getidUser(headerAuth);

        // Params

        var userId = parseInt(req.body.AdminId);
        var PublicationId = parseInt(req.body.PublicationId);
        var contentText = req.body.contentText;
        var attachment = req.body.attachment;

        if (PublicationId <= 0) {
            return res.status(400).json({'error': 'invalid parameters'});
        }

        asyncLib.waterfall([
            function (done) {
                models.Publication.findOne({
                    where: {id: PublicationId}

                })
                    .then(function (messageFound) {
                        done(null, messageFound);
                    })
                    .catch(function (err) {
                        return res.status(500).json({'error': 'unable to verify message'});
                    });
            },
            function (messageFound, done) {
                if (messageFound) {
                    models.Admin.findOne({
                        where: {id: adminId}
                    })
                        .then(function (userFound) {
                            done(null, messageFound, userFound);
                        })
                        .catch(function (err) {
                            return res.status(500).json({'error': 'unable to verify user'});
                        });
                } else {
                    res.status(404).json({'error': 'post already liked'});
                }
            },
            function (messageFound, userFound, done) {
                if (userFound) {
                    models.Users.findOne({
                        where: {
                            id: userId
                        }
                    })
                        .then(function (adminFound) {
                            done(null, messageFound, userFound, adminFound);
                        })
                        .catch(function (err) {
                            return res.status(500).json({'error': 'unable to verify is user already liked'});
                        });
                } else {
                    res.status(404).json({'error': 'user not exist'});
                }
            },
            function (messageFound, userFound, adminFound, done) {
                if (adminFound) {

                    models.Chat.create({
                        AdminId: userId,
                        UserId: adminId,
                        PublicationId: PublicationId,
                        typeSender: USER,
                        contentText: contentText,
                        attachment: attachment,
                        state: 0
                    })

                        .then(function (alreadyLikeFound) {
                            done(alreadyLikeFound);
                        })
                        .catch(function (err) {
                            return res.status(500).json({'error': 'unable to set user reaction'});
                        });
                } else {

                }
            },

        ], function (alreadyLikeFound) {
            if (alreadyLikeFound) {
                return res.status(201).json(alreadyLikeFound);
            } else {
                return res.status(500).json({'error': 'cannot update message'});
            }
        });
    },

    getAdminChatList: function (req, res) {

        var headerAuth = req.headers['authorization'];
        var AdminId = jwt_User_Utils.getidUser(headerAuth);

        var fields = req.query.fields;

        var offset = parseInt(req.query.offset);
        var order = req.query.order;

        if (AdminId <= 0 || AdminId == null) {
            return res.status(400).json({'error': 'Identification failed'});
        }

        models.Chat.findAll({
            order: [(order != null) ? order.split(':') : ['createdAt', 'ASC']],
            attributes: (fields !== '*' && fields != null) ? fields.split(',') : null,
            include: [{
                model: models.Users,
                attributes: ['username'],
            }],
            offset: (!isNaN(offset)) ? offset : null,

            where: {AdminId: AdminId},

        }).then(function (publication) {
            if (publication) {
                res.status(200).json(publication);
            } else {
                res.status(404).json({"error": "no messages found"});
            }
        }).catch(function (err) {
            console.log(err);
            res.status(500).json({"error": "invalid fields"});
        });
    },

    AllAdminChatmessage: function (req, res) {
        var headerAuth = req.headers['authorization'];
        var AdminnId = jwt_User_Utils.getidUser(headerAuth);

        var fields = req.query.fields;

        var offset = parseInt(req.query.offset);
        var order = req.query.order;
        var PublicationId = parseInt(req.body.PublicationId);
        var AdminId = parseInt(req.body.AdminId);

        models.Chat.findAll({
            order: [(order != null) ? order.split(':') : ['createdAt', 'ASC']],
            attributes: (fields !== '*' && fields != null) ? fields.split(',') : null,

            offset: (!isNaN(offset)) ? offset : null,
            where: [{UserId: AdminnId}, {PublicationId: PublicationId}, {AdminId: AdminId}],

        }).then(function (publication) {
            if (publication) {
                res.status(200).json(publication);
            } else {
                res.status(404).json({"error": "no messages found"});
            }
        }).catch(function (err) {
            console.log(err);
            res.status(500).json({"error": "invalid fields"});
        });
    }
}
