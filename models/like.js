'use strict';
module.exports = (sequelize, DataTypes) => {
  const Like = sequelize.define('Like', {
    PublicationId:{
      type: DataTypes.INTEGER,
      references: {
        model: 'Publication',
        key: 'id'
      }},
    UserId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id'
      }
    },
    isLike: DataTypes.INTEGER,
    state: DataTypes.INTEGER
  }, {});
  Like.associate = function(models) {
    // associations can be defined here

    models.Users.belongsToMany(models.Publication, {
      through: models.Like,
      foreignKey: 'UserId',
      otherKey: 'PublicationId',
    });

    models.Publication.belongsToMany(models.Users, {
      through: models.Like,
      foreignKey: 'PublicationId',
      otherKey: 'UserId',
    });

    models.Like.belongsTo(models.Users, {
      foreignKey: 'UserId',
    });

    models.Like.belongsTo(models.Publication, {
      foreignKey: 'PublicationId',
    });


  };
  return Like;
};
