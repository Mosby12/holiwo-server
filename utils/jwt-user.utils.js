// Imports
var jwt = require('jsonwebtoken');

const JWT_SIGN_SECRET = '<JWT_SIGN_TOKEN>';

// Exported functions
module.exports = {
  generateTokenForUser: function(userData) {
    return jwt.sign({
      idUser: userData.id,
     state: userData.state
    },
    JWT_SIGN_SECRET,
    {
      expiresIn: '10y'

    })
  },
  parseAuthorization: function(authorization) {
    return (authorization != null) ? authorization.replace('Bearer ', '') : null;
  },
  getidUser: function(authorization) {
    var idUser = -1;
    var token = module.exports.parseAuthorization(authorization);
    if(token != null) {
      try {
        var jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
        if(jwtToken != null)
          idUser = jwtToken.idUser;
      } catch(err) { }
    }
    return idUser;
  }
}
