'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Publications', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },

      AdminId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references:{
          model:'Admins',
          key:'id'
        }},
        CategorieId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references:{
            model:'Categories',
            key:'id'
          }},
      CurrencyId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references:{
          model:'Currencies',
          key:'id'
        }},
          price: {
            type: Sequelize.FLOAT,
            allowNull: false,
          },
      rate: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      title: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      content: {
        type: Sequelize.STRING,
        allowNull: false,
      },

      attachment: {
        allowNull: true,
        type: Sequelize.STRING
      },
        attachment1: {
            allowNull: true,
            type: Sequelize.STRING
        },
        attachment2: {
            allowNull: true,
            type: Sequelize.STRING
        },
      likes: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      state: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Publications');
  }
};
