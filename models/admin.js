'use strict';
module.exports = (sequelize, DataTypes) => {
  const Admin = sequelize.define('Admin', {
    mail: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    type: DataTypes.INTEGER,
    state: DataTypes.INTEGER,
    name: DataTypes.STRING,
    firstname: DataTypes.STRING,
    tel: DataTypes.STRING,
    followers: DataTypes.INTEGER,
    address: DataTypes.STRING,
    propic: DataTypes.STRING,
    position: DataTypes.STRING,
    town: DataTypes.STRING
  }, {});
  Admin.associate = function(models) {
    // associations can be defined here

  };
  return Admin;
};
