// Imports
var jwt = require('jsonwebtoken');

const JWT_SIGN_SECRET = '<JWT_SIGN_TOKEN>';

// Exported functions
module.exports = {
  generateTokenForUser: function(userData) {
    return jwt.sign({
      idAdmin: userData.id,
     state: userData.state
    },
    JWT_SIGN_SECRET,
    {
      expiresIn: '1h'
    })
  },
  parseAuthorization: function(authorization) {
    return (authorization != null) ? authorization.replace('Bearer ', '') : null;
  },
  getidAdmin: function(authorization) {
    var idAdmin = -1;
    var token = module.exports.parseAuthorization(authorization);
    if(token != null) {
      try {
        var jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
        if(jwtToken != null)
          idAdmin = jwtToken.idAdmin;
      } catch(err) { }
    }
    return idAdmin;
  }
}
