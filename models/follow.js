'use strict';
module.exports = (sequelize, DataTypes) => {
    const Follow = sequelize.define('Follow', {
        AdminId: {
            type: DataTypes.INTEGER,
            references: {
                model: 'Admin',
                key: 'id'
            }
        },
        UserId: {
            type: DataTypes.INTEGER,
            references: {
                model: 'Users',
                key: 'id'
            }
        },
        isFollow: DataTypes.INTEGER,
        state: DataTypes.INTEGER
    }, {});
    Follow.associate = function (models) {
        // associations can be defined here

        models.Users.belongsToMany(models.Admin, {
            through: models.Follow,
            foreignKey: 'UserId',
            otherKey: 'AdminId',
        });

        models.Admin.belongsToMany(models.Users, {
            through: models.Follow,
            foreignKey: 'AdminId',
            otherKey: 'UserId',
        });

        models.Follow.belongsTo(models.Users, {
            foreignKey: 'UserId'
        });

        models.Follow.belongsTo(models.Admin, {
            foreignKey: 'AdminId'
        });


    };
    return Follow;
};
