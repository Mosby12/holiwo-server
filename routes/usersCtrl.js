// Imports
var bcrypt    = require('bcrypt');
var jwtUtils  = require('../utils/jwt-user.utils');
var models    = require('../models');
var asyncLib  = require('async');

// Constants
const EMAIL_REGEX     = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_REGEX  = /^(?=.*\d).{4,8}$/;

// Routes
module.exports = {
  register: function(req, res) {

    // Params
    var mail    = req.body.mail;
    var username = req.body.username;
    var password = req.body.password;
    var name = req.body.name;
    var firstName = req.body.firstName;
    var propic = req.body.propic;
    var address = req.body.address;
    var tel = req.body.tel;
    var position = req.body.position;
    var town = req.body.town;



    if (username.length >= 13 || username.length <= 4) {
      return res.status(403).json({'error': 'wrong username (must be length 5 - 12)'});
    }
    if (name.length <= 2 ) {
      return res.status(402).json({'error': 'wrong name (must be > 2)'});
    }
    if (firstName.length <= 3) {
      return res.status(405).json({'error': 'wrong firstname  (must be > 3)'});
    }
    if (tel.length <= 7) {
      return res.status(408).json({'error': 'tel is not valid'});
    }

    if (!EMAIL_REGEX.test(mail)) {
      return res.status(406).json({'error': 'email is not valid'});
    }

    if (!PASSWORD_REGEX.test(password)) {
      return res.status(407).json({'error': 'password invalid (must length 4 - 8 and include 1 number at least)'});
    }


    asyncLib.waterfall([
      function(done) {
        models.Users.findOne({
          attributes: ['mail'],
          where: [{mail: mail}, {state: 0}]
        })
            .then(function(userFound) {
              done(null, userFound);
            })
            .catch(function(err) {
              return res.status(500).json({ 'error': 'unable to verify user' });
            });
      },
      function(userFound, done) {
        if (!userFound) {
          bcrypt.hash(password, 5, function( err, bcryptedPassword ) {
            done(null, userFound, bcryptedPassword);
          });
        } else {
          return res.status(409).json({ 'error': 'user already exist' });
        }
      },
      function(userFound, bcryptedPassword, done) {
        var newUser = models.Users.create({
          mail: mail,
          username: username,
          password: bcryptedPassword,
          name: name,
          firstname:firstName,
          propic: propic,
          address: address,
          tel: tel,
          position:position,
          town:town,
          state: 0
        })
            .then(function(newUser) {
              done(newUser);
            })
            .catch(function(err) {
              return res.status(500).json({ 'error': 'cannot add user' });
            });
      }
    ], function(newUser) {
      if (newUser) {
        return res.status(201).json({
          'userId': newUser.id
        });
      } else {
        return res.status(500).json({ 'error': 'cannot add user' });
      }
    });
  },




  login: function(req, res) {

    // Params
    var email    = req.body.mail;
    var password = req.body.password;

    if (email == null ||  password == null) {
      return res.status(400).json({ 'error': 'missing parameters' });
    }

    asyncLib.waterfall([
      function(done) {
        models.Users.findOne({
          where: [{mail: email}, {state: 0}]
        })
            .then(function(userFound) {
              done(null, userFound);
            })
            .catch(function(err) {
              return res.status(500).json({ 'error': 'unable to verify user' });
            });
      },
      function(userFound, done) {
        if (userFound) {
          bcrypt.compare(password, userFound.password, function(errBycrypt, resBycrypt) {
            done(null, userFound, resBycrypt);
          });
        } else {
          return res.status(404).json({ 'error': 'user not exist in DB' });
        }
      },
      function(userFound, resBycrypt, done) {
        if(resBycrypt) {
          done(userFound);
        } else {
          return res.status(403).json({ 'error': 'invalid password' });
        }
      }
    ], function(userFound) {
      if (userFound) {
        return res.status(201).json({
          'userId': userFound.id,
          'token': jwtUtils.generateTokenForUser(userFound),
          'username': userFound.username,
          'firstname': userFound.firstname,
          'name': userFound.name,
          'town': userFound.town,
          'tel': userFound.tel,
          'propic': userFound.propic,
          'address': userFound.address,
          'mail': userFound.mail,
        });
      } else {
        return res.status(500).json({ 'error': 'cannot log on user' });
      }
    });
  },
  getUserProfile: function(req, res) {
    // Getting auth header
    var headerAuth  = req.headers['authorization'];
    var idUser      = jwtUtils.getidUser(headerAuth);

    if (idUser < 0)
      return res.status(400).json({ 'error': 'wrong token' });

    models.Users.findOne({
      attributes: [ 'id', 'mail', 'username', 'state','name', 'firstname', 'propic', 'tel', 'address','position','town','state','createdAt',"updatedAt" ],
      where: [{id: idUser}, {state: 0}]
    }).then(function(user) {
      if (user) {
        res.status(201).json(user);
      } else {
        res.status(404).json({ 'error': 'user not found' });
      }
    }).catch(function(err) {
      return res.status(200).json({ 'error': idUser});
      res.status(500).json({ 'error': 'cannot fetch user' });
    });
  },

  searchUserProfile: function(req, res) {
    // Getting auth header

    var idUser      =  parseInt(req.params.idUser);

    if (idUser < 0)
      return res.status(400).json({ 'error': 'wrong token' });

    models.Users.findOne({
      attributes: [ 'id', 'mail', 'username', 'state','name', 'firstname', 'propic', 'tel', 'address','position','town','state','createdAt',"updatedAt" ],
      where: [{id: idUser}, {state: 0}]
    }).then(function(user) {
      if (user) {
        res.status(201).json(user);
      } else {
        res.status(404).json({ 'error': 'user not found' });
      }
    }).catch(function(err) {
      res.status(500).json({ 'error': 'cannot fetch user' });
    });
  },

  updateUserProfilePassword: function(req, res) {
    // Getting auth header
    var headerAuth  = req.headers['authorization'];
    var idUser      = jwtUtils.getidUser(headerAuth);

    // Params
    var password = req.body.password;

    if (!PASSWORD_REGEX.test(password)) {
      return res.status(400).json({ 'error': 'password invalid (must length 4 - 8 and include 1 number at least)' });
    }

    asyncLib.waterfall([
      function(done) {
        models.Users.findOne({
          attributes: ['id', 'password'],
          where: [{id: idUser}, {state: 0}]
        }).then(function (userFound) {
          done(null, userFound);
        })
            .catch(function(err) {
              return res.status(500).json({ 'error': 'unable to verify user' });
            });
      },
      function(userFound, done) {
        if (userFound) {
          bcrypt.hash(password, 5, function( err, bcryptedPassword ) {
            done(null, userFound, bcryptedPassword);
          });
        } else {
          return res.status(409).json({ 'error': 'error crypt' });
        }
      },
      function(userFound,bcryptedPassword, done) {
        if(userFound) {
          userFound.update({
            password: (password ? bcryptedPassword: password)
          }).then(function() {
            done(userFound);
          }).catch(function(err) {
            res.status(500).json({ 'error': 'cannot update user' });
          });
        } else {
          res.status(404).json({ 'error': 'user not found' });
        }
      },
    ], function(userFound) {
      if (userFound) {
        return res.status(201).json(userFound);
      } else {
        return res.status(500).json({ 'error': 'cannot update user profile' });
      }
    });
  },

  updateUserProfileState: function(req, res) {
    // Getting auth header
    var headerAuth  = req.headers['authorization'];
    var idUser      = jwtUtils.getidUser(headerAuth);

    // Params
    var state = req.body.state;

    asyncLib.waterfall([
      function(done) {
        models.Users.findOne({
          attributes: ['id', 'state'],
          where: [{id: idUser}, {state: 0}]
        }).then(function (userFound) {
          done(null, userFound);
        })
            .catch(function(err) {
              return res.status(500).json({ 'error': 'unable to verify user' });
            });
      },
      function(userFound, done) {
        if(userFound) {
          userFound.update({
            state: (state ? state: userFound.state)
          }).then(function() {
            done(userFound);
          }).catch(function(err) {
            res.status(500).json({ 'error': 'cannot update user' });
          });
        } else {
          res.status(404).json({ 'error': 'user not found' });
        }
      },
    ], function(userFound) {
      if (userFound) {
        return res.status(201).json(userFound);
      } else {
        return res.status(500).json({ 'error': 'cannot update user profile' });
      }
    });
  },
  updateUserProfile: function(req, res) {
    // Getting auth header
    var headerAuth  = req.headers['authorization'];
    var idPeople     = jwtUtils.getidUser(headerAuth);

    // Params
    var name = req.body.name;
    var firstName = req.body.firstName;
    var propic = req.body.propic;
    var address = req.body.address;
    var tel = req.body.tel;
    var state = req.body.state;
    var position = req.body.position;
    var town = req.body.town;

    asyncLib.waterfall([

      function(done) {
        models.Users.findOne({
          attributes: ['id','name', 'firstname', 'propic', 'tel', 'address','town','position','state'],
          where: [{id: idPeople}, {state: 0}]
        }).then(function (PeopleFound) {
          done(null, PeopleFound);
        })
            .catch(function(err) {
              return res.status(500).json({ 'error': 'unable to verify admin' });
            });
      },
      function(PeopleFound, done) {
        if(PeopleFound) {
          PeopleFound.update({
            name: (name ? name: PeopleFound.name),
            firstname: (firstName ? firstName: PeopleFound.firstname),
            propic: (propic ? propic: PeopleFound.propic),
            tel: (tel ? tel: PeopleFound.tel),
            address: (address ? address: PeopleFound.address),
            state: (state ? state: PeopleFound.state),
            town: (town ? town: PeopleFound.town),
            position: (position ? position: PeopleFound.position)
          }).then(function() {
            done(PeopleFound);
          }).catch(function(err) {
            res.status(500).json({ 'error': 'cannot update admin' });
          });
        } else {
          res.status(404).json({ 'error': 'admin not found' });
        }
      },
    ], function(PeopleFound) {
      if (PeopleFound) {
        return res.status(201).json(PeopleFound);
      } else {
        return res.status(500).json({ 'error': 'cannot update admin profile' });
      }
    });
  }

}
