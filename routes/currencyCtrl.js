//Imports
var models    = require('../models');
var asyncLib  = require('async');

//Routes

module.exports ={
    saves: function(req, res) {

        //parms
        var currencyName = req.body.currencyName;
        var description = req.body.description;
        var 	symbol = req.body.symbol;



        if (currencyName == null || description == null||symbol==null) {
            return res.status(400).json({'error': 'missing parameters'});
        }
        if (currencyName.length <= 2 ) {
            return res.status(400).json({'error': 'wrong currencyName'});
        }
        if (description.length <= 4) {
            return res.status(400).json({'error': 'wrong description'});
        }
        if (symbol.length <= 0) {
            return res.status(400).json({'error': 'wrong symbol'});
        }

        asyncLib.waterfall([
            function(done) {
                models.Currency.findOne({
                    attributes: [ 'currencyName', 'description','symbol','state','createdAt',"updatedAt" ],
                    where: {currencyName: currencyName}
                }).then(function (currencyFound) {
                    done(null, currencyFound);
                })
                    .catch(function(err) {
                        return res.status(500).json({ 'error': 'unable to verify admin' });
                    });
            },
            function (currencyFound, done) {
                if (!currencyFound) {
                    models.Currency.create({
                        currencyName:currencyName,
                        description:description,
                        symbol:symbol,
                        state: 0
                    })
                        .then(function (newCurrency) {
                            done(newCurrency);
                        });
                } else {
                    return res.status(500).json({'error': 'Currency already exist'});
                }
            },
        ], function (newCurrency) {
            if (newCurrency) {
                return res.status(201).json(newCurrency);
            } else {
                return res.status(500).json({'error': 'cannot add currency'});
            }
        });
    },

    getCurrencyById: function(req, res) {
        // Getting auth header

        var CurrencyId    = req.body.CurrencyId;

        if (CurrencyId < 0)
            return res.status(400).json({ 'error': 'wrong token' });

        models.Currency.findOne({
            attributes: [ 'currencyName', 'description','state','createdAt',"updatedAt" ],
            where: { idAdmin: CurrencyId }
        }).then(function (currency) {
            if (currency) {
                res.status(201).json(currency);
            } else {
                res.status(404).json({ 'error': 'admin not found' });
            }
        }).catch(function(err) {
            res.status(500).json({'error': 'cannot fetch currency'});
        });
    },



    updateCurrency: function(req, res) {

        // Params
        var CurrencyId    = req.body.CurrencyId;
        var currencyName = req.body.currencyName;
        var description = req.body.description;
        var symbol= req.body.symbol;
        var state = req.body.state;

        asyncLib.waterfall([
            function(done) {
                models.Currency.findOne({
                    attributes: ['currencyName', 'description', 'symbol', 'state'],
                    where: { id: CurrencyId}
                }).then(function (currencyFound) {
                    done(null, currencyFound);
                })
                    .catch(function(err) {
                        return res.status(500).json({ 'error': 'unable to verify admin' });
                    });
            },
            function (currencyFound, done) {
                if (currencyFound) {
                    currencyFound.update({
                        currencyName: (currencyName ? currencyName : currencyFound.currencyName),
                        description: (description ? description : currencyFound.description),
                        symbol: (symbol ? symbol : currencyFound.symbol),
                        state: (state ? state : currencyFound.state)
                    }).then(function() {
                        done(currencyFound);
                    }).catch(function(err) {
                        res.status(500).json({ 'error': 'cannot update admin' });
                    });
                } else {
                    res.status(404).json({ 'error': 'admin not found' });
                }
            },
        ], function (currencyFound) {
            if (currencyFound) {
                return res.status(201).json(currencyFound);
            } else {
                return res.status(500).json({ 'error': 'cannot update admin profile' });
            }
        });
    }

}
