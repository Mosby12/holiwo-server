'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Chats', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      AdminId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references:{
          model:'Admins',
          key:'id'
        }
      },
      UserId: {
        type: Sequelize.INTEGER,
        allowNull: false,
            references:{
          model:'Users',
          key:'id'
        }
      },
      PublicationId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references:{
          model:'Publications',
          id:'id',
        }
      },
      typeSender: {
        type: Sequelize.STRING,
        allowNull: false
      },
      contentText: {
        type: Sequelize.STRING,
        allowNull: false
      },
      attachment: {
        type: Sequelize.STRING,
        allowNull: true
      },
      state: {
          type: Sequelize.INTEGER,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Chats');
  }
};
