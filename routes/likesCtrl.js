// Imports
var models   = require('../models');
var jwtUtils = require('../utils/jwt-user.utils');
var asyncLib = require('async');

// Constants
const DISLIKED = 0;
const LIKED    = 1;

// Routes
module.exports = {
  likePost: function(req, res) {
    // Getting auth header
    var headerAuth  = req.headers['authorization'];
    var userId      = jwtUtils.getidUser(headerAuth);

    // Params
    var PublicationId = parseInt(req.params.publishId);


    if (PublicationId <= 0) {
      return res.status(400).json({ 'error': 'invalid parameters' });
    }

      asyncLib.waterfall([
          function(done) {
              models.Publication.findOne({
                  where: { id: PublicationId }

              })
                  .then(function(messageFound) {
                      done(null, messageFound);
                  })
                  .catch(function(err) {
                      return res.status(500).json({ 'error': 'unable to verify message' });
                  });
          },
          function(messageFound, done) {
              if(messageFound) {
                  models.Users.findOne({
                      where: { id: userId }
                  })
                      .then(function(userFound) {
                          done(null, messageFound, userFound);
                      })
                      .catch(function(err) {
                          return res.status(500).json({ 'error': 'unable to verify user' });
                      });
              } else {
                  res.status(404).json({ 'error': 'post already liked' });
              }
          },
          function(messageFound, userFound, done) {
              if(userFound) {
                  models.Like.findOne({
                      where: {
                          userId: userId,
                          PublicationId: PublicationId
                      }
                  })
                      .then(function(userAlreadyLikedFound) {
                          done(null, messageFound, userFound, userAlreadyLikedFound);
                      })
                      .catch(function(err) {
                          return res.status(500).json({ 'error': 'unable to verify is user already liked' });
                      });
              } else {
                  res.status(404).json({ 'error': 'user not exist' });
              }
          },
          function(messageFound, userFound, userAlreadyLikedFound, done) {
              if(!userAlreadyLikedFound) {

                  models.Like.create({
                      PublicationId:messageFound.id,
                      isLike:LIKED,
                      UserId:userId,
                  state:0
                  })

                      .then(function (alreadyLikeFound) {
                          done(null, messageFound, userFound);
                      })
                      .catch(function(err) {
                          return res.status(500).json({ 'error': 'unable to set user reaction' });
                      });
              } else {
                  if (userAlreadyLikedFound.isLike === DISLIKED) {
                      userAlreadyLikedFound.update({
                          isLike: LIKED,
                      }).then(function() {
                          done(null, messageFound, userFound);
                      }).catch(function(err) {
                          res.status(500).json({ 'error': 'cannot update user reaction' });
                      });
                  } else {
                      res.status(409).json({ 'error': 'message already liked' });
                  }
              }
          },
          function(messageFound, userFound, done) {
              messageFound.update({
                  likes: messageFound.likes + 1,
              }).then(function() {
                  done(messageFound);
              }).catch(function(err) {
                  res.status(500).json({ 'error': 'cannot update message like counter' });
              });
          },
      ], function(messageFound) {
          if (messageFound) {
              return res.status(201).json(messageFound);
          } else {
              return res.status(500).json({ 'error': 'cannot update message' });
          }
      });
  },
  dislikePost: function(req, res) {
    // Getting auth header
    var headerAuth  = req.headers['authorization'];
    var userId      = jwtUtils.getidUser(headerAuth);

    // Params
    var publishId = parseInt(req.params.publishId);

    if (publishId <= 0) {
      return res.status(400).json({ 'error': 'invalid parameters' });
    }

    asyncLib.waterfall([
      function(done) {
        models.Publication.findOne({
          where: { id: publishId }
        })
            .then(function(messageFound) {
              done(null, messageFound);
            })
            .catch(function(err) {
              return res.status(500).json({ 'error': 'unable to verify message' });
            });
      },
      function(messageFound, done) {
        if(messageFound) {
          models.Users.findOne({
            where: { id: userId }
          })
              .then(function(userFound) {
                done(null, messageFound, userFound);
              })
              .catch(function(err) {
                return res.status(500).json({ 'error': 'unable to verify user' });
              });
        } else {
          res.status(404).json({ 'error': 'post already liked' });
        }
      },
      function(messageFound, userFound, done) {
        if(userFound) {
          models.Like.findOne({
            where: {
              userId: userId,
              PublicationId: publishId
            }
          })
              .then(function(userAlreadyLikedFound) {
                done(null, messageFound, userFound, userAlreadyLikedFound);
              })
              .catch(function(err) {
                return res.status(500).json({ 'error': 'unable to verify is user already liked' });
              });
        } else {
          res.status(404).json({ 'error': 'user not exist' });
        }
      },
      function(messageFound, userFound, userAlreadyLikedFound, done) {
        if(!userAlreadyLikedFound) {
            models.Like.create({
                PublicationId:messageFound.id,
                isLike:DISLIKED,
                UserId:userId,
                state:0
            })
              .then(function (alreadyLikeFound) {
                done(null, messageFound, userFound);
              })
              .catch(function(err) {
                return res.status(500).json({ 'error': 'unable to set user reaction' });
              });
        } else {
          if (userAlreadyLikedFound.isLike === LIKED) {
            userAlreadyLikedFound.update({
              isLike: DISLIKED,
            }).then(function() {
              done(null, messageFound, userFound);
            }).catch(function(err) {
              res.status(500).json({ 'error': 'cannot update user reaction' });
            });
          } else {
            res.status(409).json({ 'error': 'message already disliked' });
          }
        }
      },
      function(messageFound, userFound, done) {
        messageFound.update({
          likes: messageFound.likes - 1,
        }).then(function() {
          done(messageFound);
        }).catch(function(err) {
          res.status(500).json({ 'error': 'cannot update message like counter' });
        });
      },
    ], function(messageFound) {
      if (messageFound) {
        return res.status(201).json(messageFound);
      } else {
        return res.status(500).json({ 'error': 'cannot update message' });
      }
    });
  },

    getUserAction: function(req, res) {
        // Getting auth header
        var headerAuth  = req.headers['authorization'];
        var userId      = jwtUtils.getidUser(headerAuth);

        // Params
        var publishId = parseInt(req.params.publishId);

        if (publishId <= 0) {
            return res.status(400).json({ 'error': 'invalid parameters' });
        }

        asyncLib.waterfall([
            function(done) {
                models.Publication.findOne({
                    where: [{id: publishId}, {state: 0}]
                })
                    .then(function(messageFound) {
                        done(null, messageFound);
                    })
                    .catch(function(err) {
                        return res.status(500).json({ 'error': 'unable to verify message' });
                    });
            },
            function(messageFound, done) {
                if(messageFound) {
                    models.Users.findOne({
                        where: [{id: userId}, {state: 0}]
                    })
                        .then(function(userFound) {
                            done(null, messageFound, userFound);
                        })
                        .catch(function(err) {
                            return res.status(500).json({ 'error': 'unable to verify user' });
                        });
                } else {
                    res.status(404).json({ 'error': 'post already liked' });
                }
            },
            function(messageFound, userFound, done) {
                if(userFound) {
                    models.Like.findOne({
                        where: [{
                            userId: userId
                        },
                            {PublicationId: publishId},
                            {state: 0}]
                    })
                        .then(function(userAlreadyLikedFound) {
                            if (userAlreadyLikedFound) {
                                res.status(200).json(userAlreadyLikedFound);
                            } else {
                                res.status(201).json({'error': 'no action on this post'});
                            }

                        })
                        .catch(function(err) {
                            return res.status(500).json({ 'error': 'unable to verify is user already liked' });
                        });
                } else {
                    res.status(404).json({ 'error': 'user not exist' });
                }
            },


        ]);
}
}
