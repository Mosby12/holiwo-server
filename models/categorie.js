'use strict';
module.exports = (sequelize, DataTypes) => {
  const Categorie = sequelize.define('Categorie', {
    categorieName: DataTypes.STRING,
    description: DataTypes.STRING,
    pic: DataTypes.STRING,
    state: DataTypes.INTEGER
  }, {});
  Categorie.associate = function(models) {
    // associations can be defined here
  };
  return Categorie;
};
