'use strict';
module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('Users', {
    name: DataTypes.STRING,
    firstname: DataTypes.STRING,
    tel: DataTypes.STRING,
    address: DataTypes.STRING,
    propic: DataTypes.STRING,
    mail: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    state: DataTypes.INTEGER,
    position: DataTypes.STRING,
    town: DataTypes.STRING
  }, {});
  Users.associate = function(models) {
    // associations can be defined here

  };
  return Users;
};
