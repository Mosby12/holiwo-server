// Imports
var bcrypt    = require('bcrypt');
var jwtUtils  = require('../utils/jwt-admin.utils');
var models    = require('../models');
var asyncLib  = require('async');

// Constants
const EMAIL_REGEX     = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_REGEX  = /^(?=.*\d).{4,8}$/;

// Routes
module.exports = {

  register: function(req, res) {

    // Params
    var mail    = req.body.mail;
    var username = req.body.username;
    var password = req.body.password;
    var type     = req.body.type;
    var name = req.body.name;
    var firstName = req.body.firstName;
    var propic = req.body.propic;
    var address = req.body.address;
    var tel = req.body.tel;
    var position = req.body.position;
    var town = req.body.town;

    if (mail == null || username == null || password == null) {
      return res.status(400).json({ 'error': 'missing parameters' });
    }
    if (name == null || firstName == null || address == null || tel == null) {
      return res.status(400).json({'error': 'missing parameters'});
    }
    if (name.length <= 2 ) {
      return res.status(400).json({'error': 'wrong name (must be > 2)'});
    }
    if (firstName.length <= 4) {
      return res.status(400).json({'error': 'wrong firstname  (must be > 4)'});
    }

    if (username.length >= 13 || username.length <= 4) {
      return res.status(400).json({ 'error': 'wrong username (must be length 5 - 12)' });
    }

    if (!EMAIL_REGEX.test(mail)) {
      return res.status(400).json({ 'error': 'email is not valid' });
    }

    if (!PASSWORD_REGEX.test(password)) {
      return res.status(400).json({ 'error': 'password invalid (must length 4 - 8 and include 1 number at least)' });
    }

    asyncLib.waterfall([
      function(done) {
        models.Admin.findOne({
          attributes: ['mail'],
          where: [{mail: mail}, {state: 0}]
        })
            .then(function(userFound) {
              done(null, userFound);
            })
            .catch(function(err) {
              return res.status(500).json({ 'error': 'unable to verify user' });
            });
      },
      function(userFound, done) {
        if (!userFound) {
          bcrypt.hash(password, 5, function( err, bcryptedPassword ) {
            done(null, userFound, bcryptedPassword);
          });
        } else {
          return res.status(409).json({ 'error': 'user already exist' });
        }
      },
      function(userFound, bcryptedPassword, done) {
        var newAdmin = models.Admin.create({
          mail: mail,
          username: username,
          password: bcryptedPassword,
          type: type,
          name: name,
          firstname:firstName,
          propic: propic,
          address: address,
          tel: tel,
          position:position,
          town:town,
          followers: 0,
          state: 0
        })
            .then(function(newAdmin) {
              done(newAdmin);
            })
            .catch(function(err) {
              return res.status(500).json({ 'error': 'cannot add user' });
            });
      }
    ], function(newAdmin) {
      if (newAdmin) {
        return res.status(201).json({
          'idAdmin': newAdmin.id
        });
      } else {
        return res.status(500).json({ 'error': 'cannot add user' });
      }
    });
  },




  login: function(req, res) {

    // Params
    var mail    = req.body.mail;
    var password = req.body.password;

    if (mail == null ||  password == null) {
      return res.status(400).json({ 'error': 'missing parameters' });
    }

    asyncLib.waterfall([
      function(done) {
        models.Admin.findOne({
          where: [{mail: mail}, {state: 0}]
        })
            .then(function(userFound) {
              done(null, userFound);
            })
            .catch(function(err) {
              return res.status(500).json({ 'error': 'unable to verify user' });
            });
      },
      function(userFound, done) {
        if (userFound) {
          bcrypt.compare(password, userFound.password, function(errBycrypt, resBycrypt) {
            done(null, userFound, resBycrypt);
          });
        } else {
          return res.status(404).json({ 'error': 'user not exist in DB' });
        }
      },
      function(userFound, resBycrypt, done) {
        if(resBycrypt) {
          done(userFound);
        } else {
          return res.status(403).json({ 'error': 'invalid password' });
        }
      }
    ], function(userFound) {
      if (userFound) {
        return res.status(201).json({
          'idAdmin': userFound.id,
          'token': jwtUtils.generateTokenForUser(userFound)
        });
      } else {
        return res.status(500).json({ 'error': 'cannot log on user' });
      }
    });
  },
  getUserProfile: function(req, res) {
    // Getting auth header
    var headerAuth  = req.headers['authorization'];
    var idAdmin      = jwtUtils.getidAdmin(headerAuth);

    if (idAdmin < 0){
      return res.status(400).json({ 'error': 'wrong token' });
    }

    models.Admin.findOne({
      attributes: ['id', 'mail', 'username', 'name', 'firstname', 'propic', 'tel', 'address', 'town', 'position', 'followers', 'state', 'createdAt', "updatedAt"],
      where: [{id: idAdmin}, {state: 0}]
    }).then(function(admin) {
      if (admin) {
        res.status(201).json(admin);
      } else {
        res.status(404).json({ 'error': 'user not found' });
      }
    }).catch(function(err) {
      res.status(500).json({ 'error': 'cannot fetch user' });
    });
  },

  searchAdminProfile : function(req, res) {
    // Getting auth header

    var idAdmin      =  parseInt(req.params.idAdmin);

    if (idAdmin < 0){
      return res.status(400).json({ 'error': 'wrong token' });
    }

    models.Admin.findOne({
      attributes: ['id', 'mail', 'username', 'name', 'firstname', 'propic', 'tel', 'address', 'town', 'position', 'followers', 'state', 'createdAt', "updatedAt"],
      where: [{id: idAdmin}, {state: 0}]
    }).then(function(admin) {
      if (admin) {
        res.status(201).json(admin);
      } else {
        res.status(404).json({ 'error': 'user not found' });
      }
    }).catch(function(err) {
      res.status(500).json({ 'error': 'cannot fetch user' });
    });
  },

  updateUserProfilePassword: function(req, res) {
    // Getting auth header
    var headerAuth  = req.headers['authorization'];
    var idAdmin      = jwtUtils.getidAdmin(headerAuth);

    // Params
    var password = req.body.password;

    if (!PASSWORD_REGEX.test(password)) {
      return res.status(400).json({ 'error': 'password invalid (must length 4 - 8 and include 1 number at least)' });
    }

    asyncLib.waterfall([
      function(done) {
        models.Admin.findOne({
          attributes: ['id', 'password'],
          where: [{id: idAdmin}, {state: 0}]
        }).then(function (adminFound) {
          done(null, adminFound);
        })
            .catch(function(err) {
              return res.status(500).json({ 'error': 'unable to verify user' });
            });
      },
      function(adminFound, done) {
        if (adminFound) {
          bcrypt.hash(password, 5, function( err, bcryptedPassword ) {
            done(null, adminFound, bcryptedPassword);
          });
        } else {
          return res.status(409).json({ 'error': 'error crypt' });
        }
      },
      function(adminFound,bcryptedPassword, done) {
        if(adminFound) {
          adminFound.update({
            password: (password ? bcryptedPassword: password)
          }).then(function() {
            done(adminFound);
          }).catch(function(err) {
            res.status(500).json({ 'error': 'cannot update user' });
          });
        } else {
          res.status(404).json({ 'error': 'user not found' });
        }
      },
    ], function(adminFound) {
      if (adminFound) {
        return res.status(201).json(adminFound);
      } else {
        return res.status(500).json({ 'error': 'cannot update user profile' });
      }
    });
  },

  updateAdminProfileState: function(req, res) {
    // Getting auth header
    var headerAuth  = req.headers['authorization'];
    var idAdmin      = jwtUtils.getidAdmin(headerAuth);

    // Params
    var state = req.body.state;
    var type = req.body.type;

    asyncLib.waterfall([
      function(done) {
        models.Admin.findOne({
          attributes: ['id', 'state'],
          where: [{id: idAdmin}, {state: 0}]
        }).then(function (userFound) {
          done(null, userFound);
        })
            .catch(function(err) {
              return res.status(500).json({ 'error': 'unable to verify user' });
            });
      },
      function(userFound, done) {
        if(userFound) {
          userFound.update({
            state: (state ? state: userFound.state),
            type: (type ? type: userFound.type)
          }).then(function() {
            done(userFound);
          }).catch(function(err) {
            res.status(500).json({ 'error': 'cannot update user' });
          });
        } else {
          res.status(404).json({ 'error': 'user not found' });
        }
      },
    ], function(userFound) {
      if (userFound) {
        return res.status(201).json(userFound);
      } else {
        return res.status(500).json({ 'error': 'cannot update user profile' });
      }
    });
  },

  updateAdminProfile: function(req, res) {
    // Getting auth header
    var headerAuth  = req.headers['authorization'];
    var idPeople     = jwtUtils.getidAdmin(headerAuth);

    // Params
    var name = req.body.name;
    var firstName = req.body.firstName;
    var propic = req.body.propic;
    var address = req.body.address;
    var tel = req.body.tel;
    var state = req.body.state;

    asyncLib.waterfall([
      function(done) {
        models.People.findOne({
          attributes: ['id','name', 'firstname', 'propic', 'tel', 'address','town','position','state'],
          where: [{id: idPeople}, {state: 0}]
        }).then(function (PeopleFound) {
          done(null, PeopleFound);
        })
            .catch(function(err) {
              return res.status(500).json({ 'error': 'unable to verify admin' });
            });
      },
      function(PeopleFound, done) {
        if(PeopleFound) {
          PeopleFound.update({
            name: (name ? name: PeopleFound.name),
            firstname: (firstName ? firstName: PeopleFound.firstname),
            propic: (propic ? propic: PeopleFound.propic),
            tel: (tel ? tel: PeopleFound.tel),
            address: (address ? address: PeopleFound.address),
            town: (town ? town: PeopleFound.town),
            position: (position ? position: PeopleFound.position),
            state: (state ? state: PeopleFound.state)

          }).then(function() {
            done(PeopleFound);
          }).catch(function(err) {
            res.status(500).json({ 'error': 'cannot update admin' });
          });
        } else {
          res.status(404).json({ 'error': 'admin not found' });
        }
      },
    ], function(PeopleFound) {
      if (PeopleFound) {
        return res.status(201).json(PeopleFound);
      } else {
        return res.status(500).json({ 'error': 'cannot update admin profile' });
      }
    });
  }

}
