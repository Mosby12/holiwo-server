// Imports
var models = require('../models');
var jwtUtils = require('../utils/jwt-user.utils');
var asyncLib = require('async');

// Constants
const UNFOLLOWED = 0;
const FOLLOWED = 1;

// Routes
module.exports = {
    followAdmin: function (req, res) {
        // Getting auth header
        var headerAuth = req.headers['authorization'];
        var userId = jwtUtils.getidUser(headerAuth);

        // Params
        var adminId = parseInt(req.params.adminId);


        if (adminId <= 0) {
            return res.status(400).json({'error': 'invalid parameters'});
        }

        asyncLib.waterfall([
            function (done) {
                models.Admin.findOne({
                    where: [{id: adminId}, {state: 0}]

                })
                    .then(function (messageFound) {
                        done(null, messageFound);
                    })
                    .catch(function (err) {
                        return res.status(500).json({'error': 'unable to verify admin'});
                    });
            },
            function (messageFound, done) {
                if (messageFound) {
                    models.Users.findOne({
                        where: [{id: userId}, {state: 0}]
                    })
                        .then(function (userFound) {
                            done(null, messageFound, userFound);
                        })
                        .catch(function (err) {
                            return res.status(500).json({'error': 'unable to verify user'});
                        });
                } else {
                    res.status(404).json({'error': 'post already followed'});
                }
            },
            function (messageFound, userFound, done) {
                if (userFound) {
                    models.Follow.findOne({
                        where: [{
                            userId: userId
                        },
                            {AdminId: adminId}, {state: 0}]

                    })
                        .then(function (userAlreadyFollowFound) {
                            done(null, messageFound, userFound, userAlreadyFollowFound);
                        })
                        .catch(function (err) {
                            return res.status(500).json({'error': 'unable to verify is user already followed'});
                        });
                } else {
                    res.status(404).json({'error': 'user not exist'});
                }
            },
            function (messageFound, userFound, userAlreadyFollowFound, done) {
                if (!userAlreadyFollowFound) {

                    models.Follow.create({
                        AdminId: messageFound.id,
                        isFollow: FOLLOWED,
                        UserId: userId,
                        state: 0
                    })

                        .then(function (alreadyFollowFound) {
                            done(null, messageFound, userFound);
                        })
                        .catch(function (err) {
                            return res.status(500).json({'error': 'unable to set user reaction'});
                        });
                } else {
                    if (userAlreadyFollowFound.isFollow === UNFOLLOWED) {
                        userAlreadyFollowFound.update({
                            isFollow: FOLLOWED,
                        }).then(function () {
                            done(null, messageFound, userFound);
                        }).catch(function (err) {
                            res.status(500).json({'error': 'cannot update user reaction'});
                        });
                    } else {
                        res.status(409).json({'error': 'admin already followed'});
                    }
                }
            },
            function (messageFound, userFound, done) {
                messageFound.update({
                    followers: messageFound.followers + 1,
                }).then(function () {
                    done(messageFound);
                }).catch(function (err) {
                    res.status(500).json({'error': 'cannot update admin followed counter'});
                });
            },
        ], function (messageFound) {
            if (messageFound) {
                return res.status(201).json(messageFound);
            } else {
                return res.status(500).json({'error': 'cannot update admin'});
            }
        });
    },
    unfollowAdmin: function (req, res) {
        // Getting auth header
        var headerAuth = req.headers['authorization'];
        var userId = jwtUtils.getidUser(headerAuth);

        // Params
        var adminId = parseInt(req.params.adminId);

        if (adminId <= 0) {
            return res.status(400).json({'error': 'invalid parameters'});
        }

        asyncLib.waterfall([
            function (done) {
                models.Admin.findOne({
                    where: [{id: adminId}, {state: 0}]
                })
                    .then(function (messageFound) {
                        done(null, messageFound);
                    })
                    .catch(function (err) {
                        return res.status(500).json({'error': 'unable to verify admin'});
                    });
            },
            function (messageFound, done) {
                if (messageFound) {
                    models.Users.findOne({
                        where: [{id: userId}, {state: 0}]
                    })
                        .then(function (userFound) {
                            done(null, messageFound, userFound);
                        })
                        .catch(function (err) {
                            return res.status(500).json({'error': 'unable to verify user'});
                        });
                } else {
                    res.status(404).json({'error': 'post already followed'});
                }
            },
            function (messageFound, userFound, done) {
                if (userFound) {
                    models.Follow.findOne({
                        where: [{
                            userId: userId
                        }, {
                            AdminId: adminId
                        }, {state: 0}]
                    })
                        .then(function (userAlreadyFollowFound) {
                            done(null, messageFound, userFound, userAlreadyFollowFound);
                        })
                        .catch(function (err) {
                            return res.status(500).json({'error': 'unable to verify is user already followed'});
                        });
                } else {
                    res.status(404).json({'error': 'user not exist'});
                }
            },
            function (messageFound, userFound, userAlreadyFollowFound, done) {
                if (!userAlreadyFollowFound) {
                    models.Follow.create({
                        AdminId: messageFound.id,
                        isFollow: UNFOLLOWED,
                        UserId: userId,
                        state: 0
                    })
                        .then(function (alreadyFollowFound) {
                            done(null, messageFound, userFound);
                        })
                        .catch(function (err) {
                            return res.status(500).json({'error': 'unable to set user reaction'});
                        });
                } else {
                    if (userAlreadyFollowFound.isFollow === FOLLOWED) {
                        userAlreadyFollowFound.update({
                            isFollow: UNFOLLOWED,
                        }).then(function () {
                            done(null, messageFound, userFound);
                        }).catch(function (err) {
                            res.status(500).json({'error': 'cannot update user reaction'});
                        });
                    } else {
                        res.status(409).json({'error': 'admin already unfollowed'});
                    }
                }
            },
            function (messageFound, userFound, done) {
                messageFound.update({
                    followers: messageFound.followers - 1,
                }).then(function () {
                    done(messageFound);
                }).catch(function (err) {
                    res.status(500).json({'error': 'cannot update admin followers counter'});
                });
            },
        ], function (messageFound) {
            if (messageFound) {
                return res.status(201).json(messageFound);
            } else {
                return res.status(500).json({'error': 'cannot update admin'});
            }
        });
    }

}
