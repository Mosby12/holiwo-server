//Imports
var models    = require('../models');
var asyncLib  = require('async');

//Routes

module.exports ={
   saves: function(req, res) {

       //parms
       var categorieName = req.body.categorieName;
       var description = req.body.description;
       var pic = req.body.pic;



       if (categorieName == null || description == null) {
           return res.status(400).json({'error': 'missing parameters'});
       }
       if (categorieName.length <= 2 ) {
           return res.status(400).json({'error': 'wrong categorieName or categorieName'});
       }
       if (description.length <= 4) {
           return res.status(400).json({'error': 'wrong categorieName or firstname'});
       }

    asyncLib.waterfall([
        function(done) {
            models.Categorie.findOne({
                attributes: [ 'categorieName', 'description', 'pic','state','createdAt',"updatedAt" ],
                where: [{categorieName: categorieName}, {state: 0}]
            }).then(function (categorieFound) {
                done(null, categorieFound);
            })
                .catch(function(err) {
                    return res.status(500).json({ 'error': 'unable to verify admin' });
                });
        },
        function (categorieFound, done) {
            if (!categorieFound) {
                models.Categorie.create({
                    categorieName: categorieName,
                    description:description,
                    pic: pic,
                    state: 0
                })
                    .then(function (newCategorie) {
                        done(newCategorie);
                    });
            } else {
                return res.status(500).json({'error': 'Categorie already exist'});
            }
        },
    ], function (newCategorie) {
        if (newCategorie) {
            return res.status(201).json(newCategorie);
        } else {
            return res.status(500).json({'error': 'cannot add Categorie'});
        }
    });
},

    getCategorieById: function(req, res) {
        // Getting auth header

        var CategorieId    = req.body.CategorieId;

        if (CategorieId < 0)
            return res.status(400).json({ 'error': 'wrong token' });

        models.Categorie.findOne({
            attributes: [ 'categorieName', 'description', 'pic','state','createdAt',"updatedAt" ],
            where: [{idAdmin: CategorieId}, {state: 0}]
        }).then(function (categorie) {
            if (categorie) {
                res.status(201).json(categorie);
            } else {
                res.status(404).json({ 'error': 'admin not found' });
            }
        }).catch(function(err) {
            res.status(500).json({'error': 'cannot fetch categorie'});
        });
    },



   updateCategorie: function(req, res) {

        // Params
       var CategorieId    = req.body.CategorieId;
       var categorieName = req.body.categorieName;
       var description = req.body.description;
       var pic = req.body.pic;
       var state = req.body.state;


        asyncLib.waterfall([

            function(done) {
                models.Categorie.findOne({
                    attributes: ['categorieName', 'description', 'pic', 'state'],
                    where: [{id: CategorieId}, {state: 0}]
                }).then(function (categorieFound) {
                    done(null, categorieFound);
                })
                    .catch(function(err) {
                        return res.status(500).json({ 'error': 'unable to verify admin' });
                    });
            },
            function (categorieFound, done) {
                if (categorieFound) {
                    categorieFound.update({
                        categorieName: (categorieName ? categorieName : categorieFound.categorieName),
                        description: (description ? description : categorieFound.description),
                        pic: (pic ? pic : categorieFound.pic),
                        state: (state ? state : categorieFound.state)
                    }).then(function() {
                        done(categorieFound);
                    }).catch(function(err) {
                        res.status(500).json({ 'error': 'cannot update admin' });
                    });
                } else {
                    res.status(404).json({ 'error': 'admin not found' });
                }
            },
        ], function (categorieFound) {
            if (categorieFound) {
                return res.status(201).json(categorieFound);
            } else {
                return res.status(500).json({ 'error': 'cannot update admin profile' });
            }
        });
    }
}
