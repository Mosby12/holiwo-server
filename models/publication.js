'use strict';
module.exports = (sequelize, DataTypes) => {
  const Publication = sequelize.define('Publication', {
    AdminId:{type:DataTypes.INTEGER,
      references:{
        model:'Admin',
        key:'id'
      }},
    CategorieId:{type:DataTypes.INTEGER,
      references:{
        model:'Categorie',
        key:'id'
      }},
    price: DataTypes.FLOAT,
    rate: DataTypes.FLOAT,
    content: DataTypes.STRING,
    state: DataTypes.INTEGER,
    attachment: DataTypes.STRING,
    attachment1: DataTypes.STRING,
    attachment2: DataTypes.STRING,
    likes: DataTypes.INTEGER,
    title: DataTypes.INTEGER
  }, {});
  Publication.associate = function(models) {
    // associations can be defined here

    models.Publication.belongsTo(models.Admin, {
      foreignKey: {
        allowNull: false
      }
    }),

    models.Publication.belongsTo(models.Categorie, {
      foreignKey: {
        allowNull: false
      }
    }),
        models.Publication.belongsTo(models.Currency, {
          foreignKey: {
            allowNull: false
          }
        })
  };
  return Publication;
};
